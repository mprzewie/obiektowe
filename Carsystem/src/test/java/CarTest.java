import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Marcin on 28.10.2016.
 */
public class CarTest {
    Car testCar;
    @Before
    public void setup(){
        testCar=new Car();
        testCar.move(MoveDirection.Right);
        testCar.move(MoveDirection.Forward);
        testCar.move(MoveDirection.Forward);
        testCar.move(MoveDirection.Forward);
    }

    @Test
    public void test(){

        assertEquals(testCar.getMapDirection(), MapDirection.East);
        assertEquals(4, testCar.getPosition().x );
        assertEquals(2, testCar.getPosition().y);
    }

}