import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Marcin on 10.11.2016.
 */
public class UnboundedMapTest {
    UnboundedMap map;
    Car car1;
    Car car2;
    Car car3;
    HayStack h1;
    HayStack h2;

    @Before
    public void setup() throws Exception{
        map=new UnboundedMap();
        car1=new Car(map, 10, 10);
        car2=new Car(map, 1,1);
        car3=new Car(map, 5, 5);
        h1=new HayStack(map, new Position(15,4));
    }

    @Test
    public void add() throws Exception {
        System.out.println(map.toString());
        assertEquals(true,(car3.getPosition().equals(new Position(5,5))));


    }

    @Test
    public void isOccupied() throws Exception {

    }


}