import static org.junit.Assert.*;

/**
 * Created by Marcin on 21.10.2016.
 */

import org.junit.*;
public class MapDirectionTest {

    public MapDirection testMapDir;
    @Before

    public void start(){
        testMapDir=MapDirection.East;
    }
    @Test
    public void toStringTest() throws Exception {
        assertEquals(testMapDir.toString(), "Wschód");

    }

    @Test
    public void nextTest() throws Exception {
        assertEquals(testMapDir.next(), MapDirection.South);

    }

    @Test
    public void previousTest() throws Exception {
        assertEquals(testMapDir.previous(), MapDirection.North);

    }

}