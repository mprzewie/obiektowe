/**
 * Created by Marcin on 10.11.2016.
 */
public class HayStack implements IMapElement {
    private Position position;

    public HayStack(IWorldMap map, Position position) {

        this.position = position;
        map.add(this);
    }

    public Position getPosition() {
        return position;
    }

    public String toString() {
        return "s";
    }
}
