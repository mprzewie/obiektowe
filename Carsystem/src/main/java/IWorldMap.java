import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Marcin on 04.11.2016.
 */
public interface IWorldMap  {
    boolean canMoveTo(Position position);
    boolean add(IMapElement element);
    void run(ArrayList<MoveDirection> directions);
    boolean isOccupied(Position position);
    Object objectAt(Position position);
    String toString();
}
