/**
 * Created by Marcin on 25.11.2016.
 */
public interface IPositionChangeObserver {
    boolean positionChanged(Position oldP, Position newP);
}
