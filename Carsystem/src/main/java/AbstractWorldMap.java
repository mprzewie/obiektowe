import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Marcin on 10.11.2016.
 */
public abstract class AbstractWorldMap implements IWorldMap, IPositionChangeObserver {

    //protected ArrayList<IMapElement> mapElements;
    protected HashMap<Position, IMapElement> mapElements;
    private ArrayList<Car> carsOnMap;
    protected MapVisualizer visualizer;

    public AbstractWorldMap() {
        this.mapElements = new HashMap<Position, IMapElement>();
        this.carsOnMap = new ArrayList<Car>();
        this.visualizer = new MapVisualizer();
    }


    public boolean add(IMapElement element) {
        if (!isOccupied(element.getPosition())) {
            mapElements.put(element.getPosition(), element);
            if (element instanceof Car){
                Car car=(Car)element;
                carsOnMap.add(car);
                car.addListener(this);
            }
            return true;
        } else {
            throw new IllegalArgumentException
                    (element.getPosition().toString()
                            + " is already occupied");
        }

    }

    public void run(ArrayList<MoveDirection> directions) {

        for (MoveDirection d : directions) {
            for (Car car : carsOnMap) {
                car.move(d);

            }
            System.out.println(toString());
        }


    }

    public boolean canMoveTo(Position position) {
        return (!isOccupied(position));
    }

    public boolean isOccupied(Position position) {
        return objectAt(position) != null;
    }

    public Object objectAt(Position position) {
        return mapElements.get(position);
    }

    public boolean positionChanged(Position oldPosition, Position newPosition) {
        Car car=(Car)mapElements.get(oldPosition);
        mapElements.remove(oldPosition);
        mapElements.put(newPosition, car);
        return !oldPosition.equals(newPosition);
    }
}
