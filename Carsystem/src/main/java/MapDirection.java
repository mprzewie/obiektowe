/**
 * Created by Marcin on 21.10.2016.
 */
public enum MapDirection {
    North,
    South,
    West,
    East;


    @Override
    public String toString() {
        String res="";
        switch (this){
            case North:
                res="North";
                break;
            case South:
                res="South";
                break;
            case East:
                res="East";
                break;
            default:
                res="West";
                break;

        }
        return res;
    }

    public MapDirection next(){
        MapDirection res;
        switch (this){
            case North:
                res=East;
                break;
            case South:
                res=West;
                break;
            case East:
                res=South;
                break;
            default:
                res=North;
                break;
        }
        return res;
    }

    public MapDirection previous(){
        MapDirection res;
        switch (this){
            case North:
                res=West;
                break;
            case South:
                res=East;
                break;
            case East:
                res=North;
                break;
            default:
                res=South;
                break;
        }
        return res;
    }

}
