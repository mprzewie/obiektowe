/**
 * Created by Marcin on 21.10.2016.
 */
public class Position {
    public final int x;
    public final int y;

    public Position(int x, int y) {
        this.x = x;
        this.y=y;
    }

    public String toString(){
        return "("+x+","+y+")";
    }

    public Boolean smaller(Position p){
        return p.x>=x && p.y>=y;
    }

    public Boolean larger(Position p){
        return p.x<=x && p.y <=y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Position position = (Position) o;

        if (x != position.x) return false;
        return y == position.y;

    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }

    Boolean equals(Position p){
        return smaller(p) && larger(p);
    }
    public Position add(Position p){
        return new Position(x + p.x, y+ p.y);
    }
}
