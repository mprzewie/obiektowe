import java.util.ArrayList;

/**
 * Created by Marcin on 04.11.2016.
 */
public class RectangularMap extends AbstractWorldMap{

    private final int width;
    private final int height;


    public RectangularMap(int width, int height) {
        super();
        this.width=width;
        this.height=height;

    }

    @Override
    public boolean canMoveTo(Position position) {
        return( (super.canMoveTo(position) && position.x>=0 && position.x<=width && position.y>=0 && position.y<=height));
    }


    public String toString(){
        return visualizer.dump(this, new Position(0,0), new Position(width,height));
    }

}
