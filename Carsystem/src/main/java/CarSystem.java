/**
 * Created by Marcin on 21.10.2016.
 */
public class CarSystem {

    public static void main(String args[]) {
        UnboundedMap map = new UnboundedMap();
        Car car1 = new Car(map, 1, 1);
        try {
            HayStack hayStack = new HayStack(map, new Position(2, 1));
        } catch (IllegalArgumentException e) {
            System.out.println(e.toString());
        }
        System.out.println(map.toString());
        String []moves={"r", "f", "f", "f"};
        map.run(OptionsParser.parse(moves));
        //for (IMapElement e : map.mapElements.values()) System.out.println(e.toString());


    }
}
