import java.util.ArrayList;

/**
 * Created by Marcin on 28.10.2016.
 */
public class OptionsParser {

    public static ArrayList<MoveDirection> parse(String [] args){
        ArrayList<MoveDirection> res=new ArrayList<MoveDirection>();
        for (String arg: args){
            if(arg.equals("f")||arg.equals("forward"))
                res.add(MoveDirection.Forward);
            else if(arg.equals("b")||arg.equals("backward"))
                res.add(MoveDirection.Backward);
            else if(arg.equals("r")||arg.equals("right"))
                res.add(MoveDirection.Right);
            else if (arg.equals("l")||arg.equals("left"))
                res.add(MoveDirection.Left);
            else throw new IllegalArgumentException(arg + " is not legal move specification");
        }
        return res;
    }
}
