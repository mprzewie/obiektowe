/**
 * Created by Marcin on 21.10.2016.
 */
public enum MoveDirection {
    Forward,
    Backward,
    Right,
    Left
}
