import javafx.geometry.Pos;

import java.util.ArrayList;

/**
 * Created by Marcin on 28.10.2016.
 */
public class Car implements IMapElement {



    private Position position;
    private MapDirection mapDirection;
    private IWorldMap map;
    private ArrayList<IPositionChangeObserver> listeners=new ArrayList<IPositionChangeObserver>();

    public Car() {
        mapDirection=MapDirection.North;
        position=new Position(2,2);
    }

    public Car(IWorldMap map){
        this.map=map;
        mapDirection=MapDirection.North;
        position=new Position(0,0);
        map.add(this);

    }

    public Car(IWorldMap map, int x, int y){
        this.map=map;
        mapDirection=MapDirection.North;
        position=new Position(x,y);
        map.add(this);
    }


    public void move(MoveDirection moveDirection){
        switch (moveDirection){
            case Right:
                mapDirection=mapDirection.next();
                break;
            case Left:
                mapDirection=mapDirection.previous();
                break;
            case Forward:
                relocate(mapDirection, 1);
                break;
            case Backward:
                relocate(mapDirection, -1);
                break;
        }
    }

    private void relocate(MapDirection dir, int distance){
        Position newPosition;
        switch (dir){
            case East:
                newPosition=position.add(new Position(distance, 0));
                break;
            case West:
                newPosition=position.add(new Position(-distance, 0));
                break;
            case South:
                newPosition=position.add(new Position(0, -distance));
                break;
            case North:
                newPosition=position.add(new Position(0, distance));
                break;
            default:
                newPosition=new Position(0,0);
        }
        if(map.canMoveTo(newPosition)){
            positionChanged(position, newPosition);
            position=newPosition;
        }
    }

    public String toString(){
        return mapDirection.toString().substring(0,1);
    }

    public Position getPosition() {
        return position;
    }

    public MapDirection getMapDirection() {
        return mapDirection;
    }

    void addListener(IPositionChangeObserver observer){
        listeners.add(observer);
    }
    void removeListener(IPositionChangeObserver observer){
        listeners.remove(observer);
    }

    private void positionChanged(Position oldPosition, Position newPosition){
        for (IPositionChangeObserver observer : listeners){
            observer.positionChanged(oldPosition,newPosition);
        }
    }
}
