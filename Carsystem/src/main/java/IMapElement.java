import javafx.geometry.Pos;

/**
 * Created by Marcin on 10.11.2016.
 */
public interface IMapElement {
    Position getPosition();
}
