import java.util.ArrayList;

/**
 * Created by Marcin on 10.11.2016.
 */
public class UnboundedMap extends AbstractWorldMap{

    public UnboundedMap() {
        super();
    }

    public String toString(){
        int minX=0;
        int minY=0;
        int width=0;
        int height=0;
        for(IMapElement e: mapElements.values()){

            int x=e.getPosition().x;
            int y=e.getPosition().y;
            if(width==0 && height==0){
                minX=x;
                minY=y;
                width=1;
                height=1;
            }
            if(x<minX){
                width+=(minX-x);
                minX=x;
            }
            else if(x>(minX+width)){
                width=(x-minX);
            }
            if(y<minY){
                height+=(minY-y);
                minY=y;
            }
            else if(y>(minY+height)){
                width=(y-minY);
            }

        }
        return visualizer.dump(this, new Position(minX,minY), new Position(width+1,height+1));
    }
}
