import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Marcin on 02.12.2016.
 */
public class ArgumentParser {

    public static ArrayList<Object> printableObjects(String args[]) {

        String artOption = "-art";
        String chapOption = "-chap";
        String wrongOptionMessage = "Wrong options! Legal options are:\n" +
                "[filepath to constitution]\n" +
                "[filepath to constitution] -chap [number of chapter]\n" +
                "[filepath to constitution] -art [number of article]\n" +
                "[filepath to constitution] -art [number of first article in set] [number of last article in set]0";
        ArrayList<Object> result = new ArrayList<Object>();

        try {
            String filepath;
            if (args.length >0 ) {
                filepath = args[0];
            } else throw new IndexOutOfBoundsException(wrongOptionMessage);
            Constitution constitution = new Constitution(filepath);
            if (args.length == 1) {
                result.add(constitution);
            }
            if (args[1].equals(artOption)) {
                int firstArt = Integer.parseInt(args[2]);
                int lastArt = firstArt;
                if (args.length > 3) {
                    lastArt = Integer.parseInt(args[3]);
                }
                for (int i = firstArt; i <= lastArt; i++) {
                    result.add(constitution.getArticleByNumber(i));
                }
            }
            else if (args[1].equals(chapOption)) {
                int chapNumber = Integer.parseInt(args[2]);
                result.add(constitution.getChapterByNumber(chapNumber));
            }
            else System.out.println(wrongOptionMessage);

        } catch (IOException e) {
            System.out.println("No such file as " + e.getMessage());
        } catch (IndexOutOfBoundsException e) {
            System.out.println(wrongOptionMessage);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return result;


    }
}
