import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by marcin on 11/28/16.
 */
public class Parser {

    private static String removeReduntants(String text, String reduntant){
        ArrayList<String> stuffIWant=
                new ArrayList<String>(Arrays.asList(text.split(reduntant)));
        String result="";
        for(String stuff :stuffIWant){
            result+=stuff;
        }
        return result;
    }

    private static String glueLines(String text){
        ArrayList<String> stuffIWant=
                new ArrayList<String>(Arrays.asList(text.split("\n")));
        String result=" ";
        for(String stuff :stuffIWant){
            if(result.endsWith(".") || stuff.charAt(1)==')' || stuff.charAt(2)==')'){
                result+="\n";
            }
            else if(!(result.endsWith(" "))){
                result+=" ";
            }
            result+=stuff;
        }
        return result.substring(1);
    }



    static String parse(String text){
        String newText=removeReduntants(text,"©Kancelaria Sejmu\n2009-11-16\n");
        newText=removeReduntants(newText, "\nO\ns\nr\n2\n3\np\nN");
        newText=removeReduntants(newText, "-\n");
        newText=glueLines(newText);
        return newText;
    }
}
