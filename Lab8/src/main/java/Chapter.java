import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by marcin on 11/28/16.
 */
public class Chapter {
    private int number;
    private String title;
    private ArrayList<Article> articles=new ArrayList<Article>();


    public Chapter(int number, String chapterText) {
        this.number=number;
        ArrayList<String> articlesString=new ArrayList<String>(Arrays.asList(chapterText.split("Art. ")));
        setTitle(articlesString.get(0).substring(chapterText.indexOf("\n")+1));
        articlesString.remove(0);
        setArticles(articlesString);
    }

    private void setTitle(String title){
        this.title=title.substring(0, title.indexOf("\n"));
    }

    private void setArticles(ArrayList<String> articlesString){
        int firstArticleNumber=Integer.parseInt(articlesString.get(0).substring(0, articlesString.get(0).indexOf(".")));
        for(int i=0; i<articlesString.size(); i++){
            Article article=new Article(firstArticleNumber+i, articlesString.get(i));
            articles.add(article);
        }

    }

    public String toString(){

        StringBuilder stringBuilder=new StringBuilder("ROZDZIAŁ "+
                Integer.toString(number)+
                ":\n"+
                title);

        for (Article article : articles){
            stringBuilder.append("\n"+article.toString());
        }
        return stringBuilder.toString();
    }

    public ArrayList<Article> getArticles() {
        return articles;
    }

    public int getFirstArticleNumber() {
        return articles.get(0).getNumber();
    }
    public int getLastArticleNumber(){
        return articles.get(articles.size()-1).getNumber();
    }
}
