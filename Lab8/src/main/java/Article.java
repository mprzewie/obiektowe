/**
 * Created by marcin on 11/28/16.
 */
public class Article {
    private int number;
    private String articleText;
    public Article(int number, String articleText) {
        this.number=number;
        this.articleText=Parser.parse(articleText.substring(articleText.indexOf("\n")+1));
            }

    public int getNumber(){
        return number;
    }

    public String getArticleText(){
        return articleText;
    }
    public String toString(){
        return "Art. "+
                Integer.toString(number)+
                ".\n"+
                articleText+
                "\n";
    }
}
