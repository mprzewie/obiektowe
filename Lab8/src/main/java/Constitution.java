import com.sun.javaws.exceptions.InvalidArgumentException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by marcin on 11/28/16.
 */
public class Constitution {

    private String preamble;
    private ArrayList<Chapter> chapters=new ArrayList<Chapter>();


    public Constitution(String filePath) throws IOException {
        String docText="";
            docText = new String(Files.readAllBytes(Paths.get(filePath)));

        ArrayList<String>chaptersString=new ArrayList<>(Arrays.asList(docText.split("Rozdział")));
        setPreamble(chaptersString.get(0));
        chaptersString.remove(0);
        setChapters(chaptersString);
    }

    private void setChapters(ArrayList<String> chaptersString ){

        for(int i=0; i<chaptersString.size(); i++){
            Chapter chapter=new Chapter(i+1, chaptersString.get(i));
            chapters.add(chapter);
        }
    }

    private void setPreamble(String preamble){
        this.preamble=Parser.parse(preamble);
    }

    public Chapter getChapterByNumber(int number){
        if (number<1 || number>(chapters.size()+1)){
            throw new IllegalArgumentException("No such chapter as "+Integer.toString(number));
        }
        return chapters.get(number-1);
    }

    public Article getArticleByNumber(int number){
        int firstArticleNumber=1;
        int lastArticleNumber=chapters.get(chapters.size()-1).getLastArticleNumber();
        if(number<firstArticleNumber || number>lastArticleNumber){
            throw new IllegalArgumentException("No such article as "+Integer.toString(number));
        }
        Chapter containing=null;
        for(Chapter chapter : chapters){
            if(chapter.getFirstArticleNumber()<=number){
                containing=chapter;
            }
        }
        return containing.getArticles().get(number-containing.getFirstArticleNumber());

    }

    public String getPreamble() {
        return preamble;
    }


    public String toString(){
        StringBuilder stringBuilder=new StringBuilder(getPreamble());
        for(Chapter chapter :chapters){
            stringBuilder.append("\n"+chapter.toString());
        }
        return stringBuilder.toString();
    }
}