import com.sun.org.apache.xpath.internal.Arg;

import static org.junit.Assert.*;

/**
 * Created by Marcin on 07.12.2016.
 */
public class ArgumentParserTest {
    String args[];
    Constitution constitution;
    String filepath="konstytucja.txt";
    int articleNum=1;
    int chapNum=2;

    @org.junit.Before
    public void setUp() throws Exception {
        constitution=new Constitution(filepath);
        args= new String[]{filepath, "-art", Integer.toString(articleNum)};

    }

    @org.junit.Test
    public void printableObjects() throws Exception {
        assertTrue(ArgumentParser.printableObjects(args).get(0).toString().equals(constitution.getArticleByNumber(articleNum).toString()));
    }

}