/**
 * Created by Marcin on 12.12.2016.
 */

import com.google.gson.*;
import com.google.gson.internal.LinkedTreeMap;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.*;

/**
 * Ja: DAO to zła nazwa, musze wymyśliś nową.
 * Jak się nazywa coś, co pobiera dużo rzeczy z Internetu?
 * Michał Sokół: Pirat
 * ...
 * Ja: APirate it is.
 */
public class APIrate {
    private String APIbaseUrl = "https://api-v3.mojepanstwo.pl/dane/poslowie";
    private String urlClosure = ".json";
    private String layerIdentifier = "layers[]=";

    public ArrayList<LinkedTreeMap<String, LinkedTreeMap>> getMPs(int term) throws IOException {

        Gson gson = new Gson();
        String url = APIbaseUrl + urlClosure + "?conditions[poslowie.kadencja]=" + Integer.toString(term);
        ArrayList<LinkedTreeMap<String, LinkedTreeMap>> result = new ArrayList<LinkedTreeMap<String, LinkedTreeMap>>();
        while (url != null) {
            JsonObject object = jsonFromURL(url);
            ArrayList<LinkedTreeMap<String, LinkedTreeMap>> array = gson.fromJson(object.get("Dataobject"), ArrayList.class);
            for (LinkedTreeMap map : array) {
                result.add(map);
            }
            LinkedTreeMap<String, String> links = gson.fromJson(object.get("Links"), LinkedTreeMap.class);
            url = links.get("next");
        }
        return result;
    }

    public LinkedTreeMap getPersonJson(int id, ArrayList<String> layers) throws IOException {
        Gson gson = new Gson();
        String url = APIbaseUrl + "/" + Integer.toString(id) + urlClosure;
        if (layers.size() > 0) url += "?";
        for (String layer : layers) {
            url += layerIdentifier + layer + "&";
        }
        JsonObject object = jsonFromURL(url);
        LinkedTreeMap<String, LinkedTreeMap> map = gson.fromJson(object, LinkedTreeMap.class);
        return map;

    }


    private JsonObject jsonFromURL(String urlString) throws IOException {
        BufferedReader reader = null;
        try {
            URL url = new URL(urlString);
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringBuilder stringBuilder = new StringBuilder();
            int read;
            char[] chars = new char[4096];
            while ((read = reader.read(chars)) != -1)
                stringBuilder.append(chars, 0, read);
            return (JsonObject) new JsonParser().parse(stringBuilder.toString());
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
    }
}
