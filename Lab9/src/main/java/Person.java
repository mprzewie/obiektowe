import com.google.gson.internal.LinkedTreeMap;

import java.io.IOException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Marcin on 14.12.2016.
 */
public class Person {


    private int id = 0;
    private String name;
    private ArrayList<Expense> expenses = null;
    private ArrayList<Trip> trips = null;
    private String officeRepairsArgument="officeRepairs";
    private String officeRepairs="Koszty drobnych napraw i remontów lokalu biura poselskiego";

    //JSON constructor
    public Person(LinkedTreeMap<String, LinkedTreeMap> json) {

        LinkedTreeMap<String, String> data = json.get("data");
        this.name = data.get("poslowie.nazwa");
        this.id = Integer.decode(data.get("poslowie.id"));
        setExpenses(json);
        setTrips(json);
    }

    public int getID() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return id + " " + name;
    }

    public float totalExpenses(String reason){
        float result=0;
        List<Expense> expenses=getExpenses();
        if(reason.equals(officeRepairsArgument)){
            expenses=expenses.stream().filter(expense -> expense.getReason().equals(officeRepairs)).collect(Collectors.toList());

        }else if(!reason.equals("")){
            throw new IllegalArgumentException("Can't filter expenses based on "+reason);
        }
        for (Expense expense : expenses){
            result+=expense.getValue();
        }
        return result;
    }

    public ArrayList<Expense> getExpenses() {
        if (expenses == null) {
            ArrayList<String> layers = new ArrayList<String>();
            layers.add("wydatki");
            APIrate apIrate = new APIrate();
            try {
                setExpenses(apIrate.getPersonJson(id, layers));
            } catch (IOException e) {
                System.out.println("Couldn't find expenses of" + name + "!");
            }
        }
        return expenses;
    }

    public ArrayList<Trip> getTrips() {
        if (trips == null) {
            ArrayList<String> layers = new ArrayList<String>();
            layers.add("wyjazdy");
            APIrate apIrate = new APIrate();
            try {
                setTrips(apIrate.getPersonJson(id, layers));
            } catch (IOException e) {
                System.out.println("Couldn't find trips of" + name + "!");
            }
        }
        return trips;
    }

    private void setExpenses(LinkedTreeMap<String, LinkedTreeMap> json) {
        LinkedTreeMap layers = json.get("layers");
        ArrayList<Expense> result = new ArrayList<Expense>();
        if (layers != null) {
            LinkedTreeMap<String, ArrayList> expensesMap = (LinkedTreeMap<String, ArrayList>) layers.get("wydatki");
            if (expensesMap != null) {
                ArrayList<LinkedTreeMap<String, String>> points = expensesMap.get("punkty");
                ArrayList<LinkedTreeMap<String, LinkedTreeMap>> years = expensesMap.get("roczniki");
                for (LinkedTreeMap year : years) {
                    int yearNumber = Integer.decode((String) year.get("rok"));
                    ArrayList<String> fields = (ArrayList<String>) year.get("pola");
                    for (int i = 0; i < points.size(); i++) {
                        Float value = Float.parseFloat(fields.get(i));
                        String reason = points.get(i).get("tytul");

                        Expense expense = new Expense(value, reason, yearNumber);
                        result.add(expense);
                    }
                }
                this.expenses = result;
            }
        }
    }

    private void setTrips(LinkedTreeMap<String, LinkedTreeMap> json) {
        LinkedTreeMap layers = json.get("layers");
        ArrayList<Trip> result = new ArrayList<Trip>();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        if (layers != null) {
            ArrayList<LinkedTreeMap> tripsMap;
            if(layers.get("wyjazdy") instanceof LinkedTreeMap){
                tripsMap=new ArrayList<>(); //this happens when the list of trips exists but is empty
            } else {
                tripsMap=(ArrayList<LinkedTreeMap>) layers.get("wyjazdy");
            }
            if (tripsMap != null) {
                for (LinkedTreeMap<String, String> tripMap : tripsMap) {
                    try {
                        String destinationCountry = tripMap.get("kraj");
                        Date dateStart = format.parse(tripMap.get("od"));
                        Date dateEnd = format.parse(tripMap.get("do"));
                        float cost=Float.parseFloat(tripMap.get("koszt_suma"));
                        Trip trip = new Trip(destinationCountry, dateStart, dateEnd,cost);
                        result.add(trip);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                this.trips = result;
            }
        }

    }
}
