
/**
 * Created by Marcin on 12.12.2016.
 */
public class Main {
    public static void main(String[] args) {

        QueryInterpreter interpreter = new QueryInterpreter();

        try {
            System.out.println(interpreter.interpret(args));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


    }


}
