import com.google.gson.internal.LinkedTreeMap;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by Marcin on 20.12.2016.
 */
public class Runner {
    public Runner() {
        int threads=50;
        System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", Integer.toString(threads));
    }

    public String totalExpenses(ArrayList<Person> mps, String mpName, String reason) {
        try {
            Person person = find(mpName, mps);
            float result = person.totalExpenses(reason);
            String resultString = "Total expenses of " + person.getName();
            if (!reason.equals("")) {
                resultString += " because of " + reason;
            }
            return resultString + " are " + result;
        } catch (Exception e) {
            return e.getMessage();
        }

    }

    public String avgExpenses(ArrayList<Person> mps, String reason) {
        double result = mps.parallelStream().mapToDouble(p -> p.totalExpenses(reason)).average().getAsDouble();
        String resultString = "Average expenses of all MPs";
        if (!reason.equals("")) {
            resultString += " because of " + reason;
        }
        return resultString + " are " + result;
    }

    public String mostTraveled(ArrayList<Person> mps) {
        ArrayList<Person> result;
        mps.parallelStream().forEach(Person::getTrips);
        mps.sort(new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o2.getTrips().size() - o1.getTrips().size();
            }
        });
        //Włóczykij
        Person snusmumriken = mps.get(0);
        return snusmumriken.getName() + " traveled the most - a total of " + snusmumriken.getTrips().size() + " trips";
    }

    public String mostTimebroad(ArrayList<Person> mps) {
        mps.parallelStream().forEach(Person::getTrips);
        mps.sort(new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                int timeAbroad1 = o1.getTrips().parallelStream().map(Trip::durationDays).reduce(0, ((acc, days) -> acc + days));
                int timeAbroad2 = o2.getTrips().parallelStream().map(Trip::durationDays).reduce(0, ((acc, days) -> acc + days));
                return timeAbroad2 - timeAbroad1;
            }
        });
        Person traveller = mps.get(0);
        int travellerDays = traveller.getTrips().parallelStream().map(Trip::durationDays).reduce(0, ((acc, days) -> acc + days));
        return traveller.getName() + " spent the most time abroad: " + travellerDays + " days";

    }

    public String mostCostfulTrip(ArrayList<Person> mps) {

        mps.parallelStream().forEach(Person::getTrips);
        Comparator<Trip> tripComparator = new Comparator<Trip>() {
            @Override
            public int compare(Trip o1, Trip o2) {
                return (int) Math.ceil(o2.getCost() - o1.getCost());
            }
        };
        Collections.sort(mps, new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                ArrayList<Trip> trips1 = o1.getTrips();
                ArrayList<Trip> trips2 = o2.getTrips();
                trips1.sort(tripComparator);
                trips2.sort(tripComparator);
                float mostCost1 = 0;
                float mostCost2 = 0;
                if (trips1.size() > 0) mostCost1 = trips1.get(0).getCost();
                if (trips2.size() > 0) mostCost2 = trips2.get(0).getCost();
                return (int) Math.ceil((mostCost2 - mostCost1));
            }
        });
        Person bourgeois = mps.get(0);
        ArrayList<Trip> bourgeoisTrips = bourgeois.getTrips();
        bourgeoisTrips.sort(tripComparator);
        return bourgeois.getName() + " had the most costful trip which was " + bourgeoisTrips.get(0);
    }

    public String traveledTo(ArrayList<Person> mps, String destination) {
        mps.parallelStream().forEach(Person::getTrips);
        List<Person> resultList=mps.parallelStream().filter(new Predicate<Person>() {
            @Override
            public boolean test(Person person) {
                List<Trip> trips = person.getTrips().parallelStream().
                        filter(trip -> trip.getDestinationCountry().equals(destination)).
                        collect(Collectors.toList());
                return trips.size() > 0;
            }
        }).collect(Collectors.toList());
        StringBuilder result = new StringBuilder("List of MPs who travelled to " + destination + ":");
        for (Person person : resultList) {
            result.append("\n" + person.getName());
        }

        return result.toString();
    }

    public ArrayList<Person> getMps(int term) throws IOException {
        APIrate apIrate=new APIrate();
        ArrayList<LinkedTreeMap<String, LinkedTreeMap>> mpsJson=apIrate.getMPs(term);
        List<Person> result=mpsJson.parallelStream().map(Person::new).collect(Collectors.toList());
        return (ArrayList<Person>) result;
    }

    private Person find(String name, ArrayList<Person> mps) {
        Person result = null;
        for (Person person : mps) {
            if (person.getName().equals(name)) {
                result = person;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException("No such person as " + name + " in selected term");
        }
        return result;
    }



}
