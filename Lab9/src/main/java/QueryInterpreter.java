import com.google.gson.internal.LinkedTreeMap;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Marcin on 12.12.2016.
 */
public class QueryInterpreter {

    /**
     * Possible arguments:
     * <termNumber> 'time-abroad<politicians name>' expenses [officeRepairs] -> wydatki
     * <termNumber> avg (expenses [officeRepairs))  -> wydatki
     * <termNumber> list-who most (traveled|time-abroad|costful-trip) -> wyjazdy
     * <termNumber> list-who traveled <country name> -> wyjazdy
     */

    private String baseURL = "https://api-v3.mojepanstwo.pl/dane/poslowie.json";
    private String termNumberArg = "<termNumber>";
    private String expenseArg = "expenses";
    private String officeRepairsArg = "officeRepairs";
    private String avgArg = "avg";
    private String listPeopleArg = "list-who";
    private String mostArg = "most";
    private String timeAbroadArg = "time-abroad";
    private String travelArg = "traveled";
    private String costfulTripArg = "costful-trip";
    private Runner runner = new Runner();


    public String interpret(String[] args) throws IllegalArgumentException, IOException {
        Runner runner = new Runner();
        StringBuilder result = new StringBuilder();
        StringBuilder message = new StringBuilder("I will now ");
        try {
            int term = Integer.decode(args[0]);
            System.out.println("Searching through MPs from term "+term);
            ArrayList<Person> mps = runner.getMps(term);
            if (args[1].equals(avgArg)) {
                if (args[2].equals(expenseArg)) {
                    String reason = "";
                    message.append("look for average expenses of all MPs in term " + term + " ");
                    if (args.length > 3) {
                        reason = args[3];
                        message.append("because of " + reason);
                    }
                    result.append(runner.avgExpenses(mps, reason));
                } else {
                    throw new IllegalArgumentException();
                }
            } else if (args[1].equals(listPeopleArg)) {
                if (args[2].equals(mostArg)) {
                    message.append("give the name of the MP who ");
                    if (args[3].equals(travelArg)) {
                        message.append("had the biggest number of trips");
                        result.append(runner.mostTraveled(mps));
                    } else if (args[3].equals(timeAbroadArg)) {
                        message.append("spent the most time abroad");
                        result.append(runner.mostTimebroad(mps));
                    } else if (args[3].equals(costfulTripArg)) {
                        message.append("went on the most expensive trip");
                        result.append(runner.mostCostfulTrip(mps));
                    } else {
                        throw new IllegalArgumentException();
                    }

                } else if (args[2].equals(travelArg)) {
                    String destination = args[3];
                    message.append("list all MPs who traveled to " + destination);
                    result.append(runner.traveledTo(mps, destination));
                }
            } else if(args[1].startsWith("'")){
                StringBuilder mpNameBuilder=new StringBuilder(args[1].substring(1));
                int i=2;
                while (!args[i].endsWith("'")){
                    mpNameBuilder.append(" "+args[i]);
                    i++;
                }
                mpNameBuilder.append(" "+args[i].substring(0,(args[i].length()-1)));
                i+=2;
                String mpName=mpNameBuilder.toString();
                String reason = "";
                message.append("look for expenses of " + mpName + " ");
                if (args.length > i) {
                    reason = args[i];
                    message.append("because of " + reason);
                }
                result.append(runner.totalExpenses(mps, mpName, reason));
            } else {
                throw new IllegalArgumentException();
            }
        } catch (IOException e) {
            throw e;
        } catch (Exception e) {
            throw new IllegalArgumentException(possibleArgsMsg());
        }
        System.out.println(message.toString());
        return result.toString();


    }

    private String possibleArgsMsg() {
        return "Illegal Argumets!\n" +
                "Possible arguments:\n" +
                termNumberArg + " '<politicians name>' " + expenseArg + " [" + officeRepairsArg + "]\n" +
                termNumberArg + " " + avgArg + " (" + expenseArg + " [" + officeRepairsArg + "])\n" +
                termNumberArg + " " + listPeopleArg + " " + mostArg + " (" + travelArg + "|" + timeAbroadArg + "|" + costfulTripArg + ")\n" +
                termNumberArg + " " + listPeopleArg + " " + travelArg + " <country name>\n";
    }

}
