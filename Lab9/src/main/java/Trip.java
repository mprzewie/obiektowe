
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.util.Date;

/**
 * Created by Marcin on 14.12.2016.
 */
public class Trip {

    private String destinationCountry;
    private Date start;
    private Date end;
    private float cost;

    public Trip(String destinationCountry, Date start, Date end, float cost) {
        this.destinationCountry = destinationCountry;
        this.start = start;
        this.end = end;
        this.cost = cost;
    }

    public int durationDays() {
        return (int) ChronoUnit.DAYS.between(start.toInstant(), end.toInstant());
    }

    public String toString() {
        return "Bailando in " + destinationCountry + " from " + start + " until " + end + " for " + durationDays() + " days cost " + cost;
    }

    public String getDestinationCountry() {
        return destinationCountry;
    }

    public float getCost() {
        return cost;
    }
}
