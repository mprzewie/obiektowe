

/**
 * Created by Marcin on 14.12.2016.
 */
public class Expense {

    private final float value;
    private final String reason;
    private final int year;

    public Expense(float value, String reason, int year) {
        this.value = value;
        this.reason = reason;
        this.year = year;
    }

    public float getValue() {
        return value;
    }

    public String getReason() {
        return reason;
    }

    public int getYear() {
        return year;
    }

    public String toString() {
        return reason + " in " + year + ":\n" + value;
    }




}
