import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by Marcin on 08.01.2017.
 */
public class InterpreterTest {
    private QueryInterpreter interpreter=new QueryInterpreter();
    ArrayList<String[]> queries;
    ArrayList<String> expectedResults;
    private long t1;
    private long t2;

    @BeforeEach
    void setTime(){
        t1=System.currentTimeMillis();
    }

    @BeforeEach
    void setAssertLists(){
        queries=new ArrayList<>();
        expectedResults=new ArrayList<>();
    }
    @AfterEach
    void tellTime(){
        t2=System.currentTimeMillis();
        System.out.println("This test took:");
        System.out.println(t2-t1);
    }

    //suma wydatków posła/posłanki o określonym imieniu i nazwisku
    //wysokości wydatków na 'drobne naprawy i remonty biura poselskiego' określonego posła/posłanki
    @Test
    void sumExpenses(){
        queries.add(new String[]{"8","'Szymon","Ziółkowski'","expenses"});
        queries.add(new String[]{"7","'Anna", "Bańkowska'", "expenses", "officeRepairs"});
        queries.add(new String[]{"8", "'Andrzej", "Duda'", "expenses"});
        queries.add(new String[]{"7", "'Piotr", "Paweł", "Bauć'", "expenses"});
        expectedResults.add("Total expenses of Szymon Ziółkowski are 0.0");
        expectedResults.add("Total expenses of Anna Bańkowska because of officeRepairs are 1978.38");
        expectedResults.add("No such person as Andrzej Duda in selected term");
        expectedResults.add("Total expenses of Piotr Paweł Bauć are 567528.5");
        assertALot(queries,expectedResults);

    }

    // średniej wartości sumy wydatków wszystkich posłów
    @Test
    void avgExpenses(){
        queries.add(new String[]{"7", "avg", "expenses"});
        queries.add(new String[]{"8", "avg", "expenses", "officeRepairs"});
        expectedResults.add("Average expenses of all MPs are 262663.02602834126");
        expectedResults.add("Average expenses of all MPs because of officeRepairs are 805.0941715159315");
        assertALot(queries,expectedResults);
    }

    //posła/posłanki, który wykonał najwięcej podróży zagranicznych
    @Test
    void mostTrips(){
        queries.add(new String[]{"7", "list-who", "most", "traveled"});
        queries.add(new String[]{"8", "list-who", "most", "traveled"});
        expectedResults.add("Tadeusz Iwiński traveled the most - a total of 67 trips");
        expectedResults.add("Jan Dziedziczak traveled the most - a total of 45 trips");
        assertALot(queries,expectedResults);

    }
    //posła/posłanki, który odbył najdroższą podróż zagraniczną
    @Test
    void mostCostfulTrip(){
        queries.add(new String[]{"7", "list-who", "most", "costful-trip"});
        queries.add(new String[]{"8", "list-who", "most", "costful-trip"});
        expectedResults.add("Adam Szejnfeld had the most costful trip which was Bailando in Meksyk from Sun Sep 30 00:00:00 CEST 2012 until Fri Oct 05 00:00:00 CEST 2012 for 5 days cost 27529.35");
        expectedResults.add("Witold Waszczykowski had the most costful trip which was Bailando in USA from Sun Jun 16 00:00:00 CEST 2013 until Sun Jun 23 00:00:00 CEST 2013 for 7 days cost 27305.58");
        assertALot(queries,expectedResults);
    }
    //posła/posłanki, który najdłużej przebywał za granicą
    @Test
    void mostTimeAbroad(){
        queries.add(new String[]{"7", "list-who", "most", "time-abroad"});
        queries.add(new String[]{"8", "list-who", "most", "time-abroad"});
        expectedResults.add("Tadeusz Iwiński spent the most time abroad: 205 days");
        expectedResults.add("Jan Dziedziczak spent the most time abroad: 120 days");
        assertALot(queries,expectedResults);

    }
    //listę wszystkich posłów, którzy odwiedzili Włochy
    @Test
    void traveledTo(){
        queries.add(new String[]{"7", "list-who", "traveled", "Włochy"});
        queries.add(new String[]{"8", "list-who", "traveled", "Włochy"});
        expectedResults.add("List of MPs who travelled to Włochy:\n" +
                "Anna Fotyga\n" +
                "Mariusz Grad\n" +
                "Adam Rogacki\n" +
                "Joanna Fabisiak\n" +
                "Rafał Grupiński\n" +
                "Piotr Tomański\n" +
                "Andrzej Czerwiński\n" +
                "Andrzej Buła\n" +
                "Cezary Grabarczyk\n" +
                "Jan Bury\n" +
                "Agnieszka Pomaska\n" +
                "Jan Dziedziczak\n" +
                "Beata Bublewicz\n" +
                "Grzegorz Raniewicz\n" +
                "Antoni Mężydło\n" +
                "Roman Jacek Kosecki\n" +
                "Janusz Piechociński\n" +
                "Tadeusz Iwiński\n" +
                "Ireneusz Raś\n" +
                "Grzegorz Schetyna\n" +
                "Wojciech Penkalski\n" +
                "Krzysztof Szczerski\n" +
                "Stefan Niesiołowski\n" +
                "Andrzej Gałażewski\n" +
                "Józef Zych\n" +
                "Sławomir Neumann\n" +
                "Tomasz Garbowski\n" +
                "Stanisław Wziątek\n" +
                "Andrzej Biernat\n" +
                "Jakub Rutnicki\n" +
                "Stanisława Prządka\n" +
                "Adam Abramowicz\n" +
                "Jacek Falfus\n" +
                "Cezary Kucharski\n" +
                "Krystyna Skowrońska\n" +
                "Anna Nemś\n" +
                "Ewa Kopacz\n" +
                "Robert Tyszkiewicz\n" +
                "Andrzej Gut-Mostowy\n" +
                "Marek Rząsa\n" +
                "Jerzy Fedorowicz\n" +
                "Bogusław Wontor\n" +
                "Eugeniusz Tomasz Grzeszczak\n" +
                "Ryszard Kalisz\n" +
                "Cezary Tomczyk\n" +
                "Michał Jaros\n" +
                "Marek Matuszewski\n" +
                "Artur Górczyński\n" +
                "Jadwiga Zakrzewska\n" +
                "Wojciech Ziemniak");
        expectedResults.add("List of MPs who travelled to Włochy:\n" +
                "Joanna Fabisiak\n" +
                "Rafał Grupiński\n" +
                "Andrzej Czerwiński\n" +
                "Cezary Grabarczyk\n" +
                "Agnieszka Pomaska\n" +
                "Jan Dziedziczak\n" +
                "Grzegorz Raniewicz\n" +
                "Antoni Mężydło\n" +
                "Roman Jacek Kosecki\n" +
                "Ireneusz Raś\n" +
                "Grzegorz Schetyna\n" +
                "Stefan Niesiołowski\n" +
                "Sławomir Neumann\n" +
                "Jakub Rutnicki\n" +
                "Adam Abramowicz\n" +
                "Jacek Falfus\n" +
                "Krystyna Skowrońska\n" +
                "Anna Nemś\n" +
                "Ewa Kopacz\n" +
                "Robert Tyszkiewicz\n" +
                "Marek Rząsa\n" +
                "Cezary Tomczyk\n" +
                "Michał Jaros\n" +
                "Marek Matuszewski\n" +
                "Wojciech Ziemniak");
        assertALot(queries,expectedResults);
    }

    private void assertALot(ArrayList<String[]> queries, ArrayList<String> expectedResults){
        if(queries.size()!=expectedResults.size()){
            throw new IllegalArgumentException("The number of queries doesn't match the number of expected results!");
        }
        for(int i=0; i<queries.size(); i++){
            try{
                String query[]=queries.get(i);
                String expectedResult=expectedResults.get(i);
                String result=interpreter.interpret(query);
                System.out.println(result);
                assertTrue(result.equals(expectedResult));
            } catch (Exception e){
                System.out.println(e.getMessage());
            }

        }


    }
}
