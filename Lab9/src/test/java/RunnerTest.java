import com.google.gson.internal.LinkedTreeMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Marcin on 20.12.2016.
 */
public class RunnerTest {

    private Runner runner;
    private ArrayList<Person> mps;
    private long t = System.currentTimeMillis();

    @Before
    public void setup() {
        runner = new Runner();
        mps = new ArrayList<>();
        APIrate apIrate = new APIrate();
        ArrayList<LinkedTreeMap<String, LinkedTreeMap>> jsonList = new ArrayList<>();
        try {
            jsonList = apIrate.getMPs(8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (LinkedTreeMap json : jsonList) {
            mps.add(new Person(json));
        }
    }

    @Test
    public void avgExpenses() throws Exception {
        System.out.println(runner.avgExpenses(mps, "officeRepairs"));
        System.out.println(runner.avgExpenses(mps, ""));
    }

    @Test
    public void totalExpenses() throws Exception {
        System.out.println(runner.totalExpenses(mps, "Anna Bańkowska", "officeRepairs"));
        System.out.println(runner.totalExpenses(mps, "Andrzej Duda", ""));
        System.out.println(runner.totalExpenses(mps, "Szymon Ziółkowski", ""));
        System.out.println(runner.totalExpenses(mps, "Zbigniew Stonoga", ""));
        System.out.println(runner.totalExpenses(mps, "Piotr Paweł Bauć", ""));
    }

    @Test
    public void mostTrips() {
        System.out.println(runner.mostTraveled(mps));
    }

    @Test
    public void mostCostfulTrip() {
        System.out.println(runner.mostCostfulTrip(mps));
    }

    @Test
    public void mostTimeAbroad() {
        System.out.println(runner.mostTimebroad(mps));

    }

    @Test
    public void traveledTo() {
        String result = runner.traveledTo(mps, "Włochy");
        System.out.println(result);
        //assert (result.contains("Andrzej Duda"));
    }



    @After
    public void time() {
        long t2 = System.currentTimeMillis();
        System.out.println("Czas: ");
        System.out.println(t2 - t);
    }

}