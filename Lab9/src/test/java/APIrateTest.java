import com.google.gson.internal.LinkedTreeMap;
import org.junit.jupiter.api.BeforeEach;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Marcin on 15.12.2016.
 */
class APIrateTest {
    APIrate APIrate;
    ArrayList<Person> mps;
    long t = System.currentTimeMillis();


    @BeforeEach
    void setup() {

        APIrate = new APIrate();
        mps = new ArrayList<Person>();
        try {
            mps = new ArrayList<Person>();
            ArrayList<LinkedTreeMap<String, LinkedTreeMap>> personJsons = APIrate.getMPs(7);
            for (LinkedTreeMap map : personJsons) {
                mps.add(new Person(map));
            }
            System.out.println(mps.size());
            for (Person p : mps) {
                System.out.println(p);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @org.junit.jupiter.api.Test
    void getMPs() {

        System.out.println(mps.size());
        assertTrue(mps.size() == 513);
        for (Person p : mps) {
            System.out.println(p.getExpenses());
        }
        long t2 = System.currentTimeMillis();
        System.out.println("Czas: ");
        System.out.println(t2 - t);
    }

}