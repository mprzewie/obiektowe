import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Created by Marcin on 15.12.2016.
 */
class DudaTest {
    APIrate APIrate;
    Person person;
    ArrayList<String> layers;

    @BeforeEach
    void setup() {
        long t = System.currentTimeMillis();
        APIrate = new APIrate();
        layers = new ArrayList<String>();
        layers.add("wydatki");
        layers.add("wyjazdy");
        try {
            person = new Person(APIrate.getPersonJson(77, layers));
        } catch (Exception e) {

            System.out.println(e);
        }
        long t2 = System.currentTimeMillis();
        System.out.println("Czas: ");
        System.out.println(t2 - t);

    }

    @Test
    void getMPs() {

        for (Expense e : person.getExpenses()) {
            System.out.println(e);
        }
        System.out.println();
        for (Trip t : person.getTrips()) {
            System.out.println(t);
        }
        System.out.println("this person's name is "+person.getName());

        System.out.println(person.totalExpenses(""));


        assertTrue(person.getName().equals("Andrzej Duda"));
    }

    @Test
    void trips(){
        List<Trip> trips = person.getTrips().parallelStream().
                filter(trip -> trip.getDestinationCountry().equals("Włochy")).
                collect(Collectors.toList());
        for(Trip trip : trips){
            System.out.println(trip);
        }
    }

}